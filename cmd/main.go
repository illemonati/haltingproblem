package main

func a(input interface{}) bool {
	if input == 1 {
		return true
	}
	return false
}

func h(functionToTest func(input interface{}) bool, functionInput interface{}) bool {
	res := functionToTest(functionInput)
	return res
}

func negate(input bool) bool {
	return !input
}

func duplicate(input interface{}) (interface{}, interface{}) {
	return input, input
}

func x(input interface{}) bool {
	i, i2 := duplicate(input)
	j := i.(func(input interface{}) bool)
	hres := h(j, i2)
	return negate(hres)
}

func main() {
	println(a(0))
	println(a(1))
	println(h(a, 0))
	println(h(a, 1))
	println(x(a))
	// cannot be done
	println(x(x))
}
