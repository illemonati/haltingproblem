GOBIN = go
buildDir = dist
flags = --trimpath -v -ldflags "-s -w"
BINNAME = halting

all: build run

build:
	${GOBIN} build ${flags} -o ${buildDir}/${BINNAME} ./cmd

run:
	./${buildDir}/${BINNAME}


